package org.example.weightedSum.xmcda;

import org.xmcda.Alternative;
import org.xmcda.CriteriaValues;
import org.xmcda.Criterion;
import org.xmcda.PerformanceTable;
import org.xmcda.ProgramExecutionResult;
import org.xmcda.ProgramParameter;
import org.xmcda.XMCDA;
import org.xmcda.utils.ValueConverters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class InputsHandler
{
	public enum AggregationOperator
	{
		WEIGHTED_SUM("weightedSum"),
		NORMALIZED_WEIGHTED_SUM("normalizedWeightedSum"),
		SUM("sum"),
		AVERAGE("average");

		private String label;

		private AggregationOperator(String operatorLabel)
		{
			label = operatorLabel;
		}

		/**
		 * Return the label for this aggregation operator
		 *
		 * @return the operator's label
		 */
		public final String getLabel()
		{
			return label;
		}

		/**
		 * Returns the operator's label
		 *
		 * @return the operator's label
		 */
		@Override
		public String toString()
		{
			return label;
		}

		/**
		 * Returns the {@link AggregationOperator} with the specified label. It behaves like {@link #valueOf(String)}
		 * with the exception
		 *
		 * @param operatorLabel the label of the constant to return
		 * @return the enum constant with the specified label
		 * @throws IllegalArgumentException if there is no aggregation operator with this label
		 * @throws NullPointerException     if operatorLabel is null
		 */
		public static AggregationOperator fromString(String operatorLabel)
		{
			if ( operatorLabel == null )
				throw new NullPointerException("operatorLabel is null");
			for ( AggregationOperator op : AggregationOperator.values() )
			{
				if ( op.toString().equals(operatorLabel) )
					return op;
			}
			throw new IllegalArgumentException("No enum AggregationOperator with label " + operatorLabel);
		}
	}


	/**
	 * This class contains every element which are needed to compute the weighted sum.
	 * It is populated by {@link InputsHandler#checkAndExtractInputs(XMCDA, ProgramExecutionResult)}.
	 */
	public static class Inputs
	{
		/** The list of active alternatives ids */
		public List<String> alternatives_ids;

		/** Contains the operator to apply in the weighted sum */
		public AggregationOperator operator;

		/** Indicates whether weights are supplied to the program */
		public boolean hasWeights;

		/** The performance table, encoded by two {@link Map Maps}:
		 * {@code { alternative's id -> { criterion's id -> value } } }.
		 *
		 * The performance table is a subset of the supplied performance table TODO COMMENT
		 */
		public Map<String, Map<String, Double>> performanceTable;

		/** The weights, encoded by a {@link Map} {@code { criterion's id -> double }} */
		public Map<String, Double> weights;
	}


	/**
	 *
	 * @param xmcda
	 * @param xmcda_exec_results
	 * @return
	 */
	static public Inputs checkAndExtractInputs(XMCDA xmcda, ProgramExecutionResult xmcda_exec_results)
	{
		Inputs inputsDict = checkInputs(xmcda, xmcda_exec_results);

		if ( xmcda_exec_results.isError() )
			return null;

		return extractInputs(inputsDict, xmcda, xmcda_exec_results);
	}


	/**
	 * Checks the inputs
	 *
	 * @param xmcda
	 * @param errors
	 * @return a map containing a key "operator" with the appropriate
	 * {@link AggregationOperator operator}
	 */
	protected static Inputs checkInputs(XMCDA xmcda, ProgramExecutionResult errors)
	{
		Inputs inputs = new Inputs();

		// Get & check the performanceTable object
		if ( xmcda.performanceTablesList.size() == 0 )
		{
			errors.addError("No performance table has been supplied");
		}
		else if ( xmcda.performanceTablesList.size() > 1 )
		{
			errors.addError("More than one performance table has been supplied");
		}
		else
		{
			@SuppressWarnings("rawtypes")
			PerformanceTable p = xmcda.performanceTablesList.get(0);

			if ( p.hasMissingValues() )
				errors.addError("The performance table has missing values");
			// Anyhow, it must be numeric here
			if ( !p.isNumeric() )
			{
				errors.addError("The performance table must contain numeric values only");
			}
			else
			{
				// convert all values as Double
				try
				{
					@SuppressWarnings("unchecked")
					PerformanceTable<Double> perfTable = p.asDouble();
					xmcda.performanceTablesList.set(0, perfTable);
				}
				catch (ValueConverters.ConversionException e)
				{
					// Should not happen because we already checked that 'p.isNumeric()' is true
					// However, bugs are always possible... so let's not ignore the throwable
					final String msg = "Error when converting the performance table's value to Double, reason:";
					errors.addError(Utils.getMessage(msg, e));
				}
			}
		}

		/* Let's check the parameters */
		check_parameters:
		{
			AggregationOperator operator = null;

			if ( xmcda.programParametersList.size() > 1 )
			{
				errors.addError("Only one programParameter is expected");
				break check_parameters;
			}

			if ( xmcda.programParametersList.size() == 0 )
			{
				errors.addError("No programParameter found");
				break check_parameters;
			}

			if ( xmcda.programParametersList.get(0).size() != 1 )
			{
				errors.addError("Parameters' list must contain exactly one element");
				break check_parameters;
			}

			final ProgramParameter<?> prgParam = xmcda.programParametersList.get(0).get(0);

			if ( ! "operator".equals(prgParam.id()) )
			{
				errors.addError(String.format("Invalid parameter w/ id '%s'", prgParam.id()));
				break check_parameters;
			}

			if ( prgParam.getValues() == null || (prgParam.getValues() != null && prgParam.getValues().size() != 1) )
			{
				errors.addError("Parameter operator must have a single (label) value only");
				break check_parameters;
			}

			try
			{
				final String operatorValue = (String) prgParam.getValues().get(0).getValue();
				operator = AggregationOperator.fromString((String) operatorValue);
			}
			catch (Throwable throwable)
			{
				StringBuffer valid_values = new StringBuffer();
				for ( AggregationOperator op : AggregationOperator.values() )
				{
					valid_values.append(op.getLabel()).append(", ");
				}
				String err = "Invalid value for parameter operator, it must be a label, ";
				err += "possible values are: " + valid_values.substring(0, valid_values.length() - 2);
				errors.addError(err);
				operator = null;
			}

			/* the following never happens: if we are here, it is present */
			// if ( ... ) errors.addError("Mandatory parameter 'operator' is missing");

			inputs.operator = operator;
		}

		/* Let's check the weights */
		check_weights:
		{
			inputs.hasWeights = xmcda.criteriaValuesList.size() >= 1;

			if ( xmcda.criteriaValuesList.size() > 1 )
			{
				errors.addError("More than one criteriaValues supplied");
				break check_weights;
			}

			/* Weights must be numeric values, independently of the parameter operator's value */
			if ( inputs.hasWeights )
			{
				final CriteriaValues<?> weights = xmcda.criteriaValuesList.get(0);
				if ( ! weights.isNumeric() )
				{
					errors.addError("The weights must be numeric values only");
					break check_weights;
				}
			}

			if ( inputs.operator == null )
				break check_weights;

			/* weights must be provided for (normalized) weighted sum */
			boolean weights_are_required = ( inputs.operator == AggregationOperator.WEIGHTED_SUM
					|| inputs.operator == AggregationOperator.NORMALIZED_WEIGHTED_SUM );

			if ( weights_are_required && ! inputs.hasWeights )
			{
				errors.addError("Criteria weights must be supplied when parameter operator is "
						                + String.format("'%s'", inputs.operator.toString()));
				break check_weights;
			}

			if ( ! weights_are_required && inputs.hasWeights )
			{
				errors.addInfo("Input weights.xml is ignored when operator is "
						               + String.format("'%s'", inputs.operator.toString()));
				break check_weights;
			}

		}


		return inputs;
	}


	/**
	 *
	 * @param inputs
	 * @param xmcda
	 * @param xmcda_execution_results
	 * @return
	 */
	protected static Inputs extractInputs(Inputs inputs, XMCDA xmcda, ProgramExecutionResult xmcda_execution_results)
	{
		// These two will be the ones on which the calculation will operate
		// criteria_ids are the ones which are active AND that are provided in the weights
    	List<String> criteria_ids = new ArrayList<>();
		// we will put the active alternatives'ids in alternatives_ids
		inputs.alternatives_ids = new ArrayList<>();

		// already converted to Double in checkInputs()
		@SuppressWarnings("unchecked")
		PerformanceTable<Double> xmcda_perf_table = (PerformanceTable<Double>) xmcda.performanceTablesList.get(0);

		// first, get the alternatives & criteria from the performance table
		for ( Alternative x_alternative: xmcda_perf_table.getAlternatives() )
        	if ( x_alternative.isActive() )
            	inputs.alternatives_ids.add(x_alternative.id());
    	for ( Criterion x_criterion: xmcda_perf_table.getCriteria() )
        	if ( x_criterion.isActive() )
            	criteria_ids.add(x_criterion.id());

	    // Check that we still have active alternatives and criteria
    	if ( inputs.alternatives_ids.size() == 0 )
        	xmcda_execution_results.addError("All alternatives of the performance table are inactive");
    	if ( criteria_ids.size() == 0 )
        	xmcda_execution_results.addError("All criteria of the performance table are inactive");

		// Last, check that criteria in weights has a non-null intersection with active criteria used in the
		// performance table
		if ( inputs.hasWeights )
		{
			List<String> criteria_ids_in_weights = new ArrayList<>();
			for ( Criterion c : xmcda.criteriaValuesList.get(0).getCriteria() )
				criteria_ids_in_weights.add(c.id());

			List<String> criteria_ids_copy = new ArrayList<>(criteria_ids);
			for ( String c_id : criteria_ids_copy )
				if ( ! criteria_ids_in_weights.contains(c_id) )
					criteria_ids.remove(c_id);
			if ( criteria_ids.size() == 0 )
				xmcda_execution_results.addError("The set of active criteria in perf.table has no common id in the " +
														 "set of the criteria ids used in the weights");
		}

		// At this point there is nothing more we can check or do but abort the execution
    	if ( xmcda_execution_results.isError() )
	        return null;

		// build performance table
		inputs.performanceTable = new LinkedHashMap<>();
		for ( Alternative x_alternative : xmcda_perf_table.getAlternatives() )
		{
			if ( ! inputs.alternatives_ids.contains(x_alternative.id()) )
				continue;
			for ( Criterion x_criterion : xmcda_perf_table.getCriteria() )
			{
				if ( !criteria_ids.contains(x_criterion.id()) )
					continue;
				// Get the value attached to the alternative, create it if necessary
				Double value = xmcda_perf_table.getValue(x_alternative, x_criterion);
				inputs.performanceTable.putIfAbsent(x_alternative.id(), new HashMap<>());
				inputs.performanceTable.get(x_alternative.id()).put(x_criterion.id(), value);
			}
		}

		// build weights
		inputs.weights = new HashMap<>();

		if ( inputs.operator == AggregationOperator.WEIGHTED_SUM ||
	         inputs.operator == AggregationOperator.NORMALIZED_WEIGHTED_SUM )
		{
			for ( Criterion x_criterion : xmcda_perf_table.getCriteria() )
			{
				if ( !criteria_ids.contains(x_criterion.id()) )
					continue;
				Double weight = null;
				try
				{
					weight = xmcda.criteriaValuesList.get(0).get(x_criterion).get(0).convertToDouble().getValue();
				}
				catch (ValueConverters.ConversionException e)
				{
					xmcda_execution_results.addError(Utils.getMessage("Conversion error", e));
					return null;
				}
				inputs.weights.put(x_criterion.id(), weight);
			}
			if ( inputs.operator == AggregationOperator.NORMALIZED_WEIGHTED_SUM )
			{
				Double weights_sum = 0.0;
				for ( Double v : inputs.weights.values() )
					weights_sum += v;
				for ( String criterion_id : inputs.weights.keySet() )
					inputs.weights.put(criterion_id, inputs.weights.get(criterion_id) / weights_sum);
			}
		}
		else
		{
			// either 'average' or 'sum'
			for ( String criterion_id : criteria_ids )
				if ( inputs.operator == AggregationOperator.AVERAGE )
					inputs.weights.put(criterion_id, 1.0 / criteria_ids.size());
				else
					inputs.weights.put(criterion_id, 1.0);
		}

		return inputs;
	}

}
