/**
 * This package contains all the necessary tools to read and write XMCDA v2 and XMCDA v3 files
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
package org.example.weightedSum.xmcda;
