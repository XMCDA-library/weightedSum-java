/**
 *
 */
package org.example.weightedSum;

import org.example.weightedSum.xmcda.InputsHandler;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Weighted sum
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class WeightedSum
{

	/**
	 * Calculates the weighted sum.
	 *
	 * @param performanceTable the input performance table encoded in two maps:
	 *                         {@code { key:alternative_id -> { key:criterion_id: value } }}
	 * @param weights the weights encode in a map {@code { key:criterion_id -> value }}.
	 * @return the weighted sum, encoded in a map {@code { key:alternative_id -> value }}
	 */
	public static Map<String, Double> calculateWeightedSum(InputsHandler.Inputs inputs)
	{
		Map<String, Map<String, Double>> performanceTable = inputs.performanceTable;
		Map<String, Double> weights = inputs.weights;

		Map<String, Double> weightedSum = new LinkedHashMap<>(); // alternative_id ->  double

		for ( String alternative_id: performanceTable.keySet() )
		{
			weightedSum.put(alternative_id, 0.0);
			for ( String criterion_id : performanceTable.get(alternative_id).keySet() )
			{
				Double temp = performanceTable.get(alternative_id).get(criterion_id) * weights.get(criterion_id);
				weightedSum.put(alternative_id, weightedSum.get(alternative_id) + temp);
			}
		}

		return weightedSum;
	}

}
