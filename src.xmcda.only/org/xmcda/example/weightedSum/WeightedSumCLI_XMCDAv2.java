/**
 *
 */
package org.xmcda.example.weightedSum;

import org.xmcda.ProgramExecutionResult;
import org.xmcda.XMCDA;
import org.xmcda.converters.v2_v3.XMCDAConverter;
import org.xmcda.example.weightedSum.Utils.InvalidCommandLineException;
import org.xmcda.parsers.xml.xmcda_v2.XMCDAParser;

import java.io.File;
import java.util.Dictionary;
import java.util.Hashtable;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class WeightedSumCLI_XMCDAv2
{
	/**
	 * @param args
	 * @throws InvalidCommandLineException 
	 */
	public static void main(String[] args) throws InvalidCommandLineException
	{
		// Parsing the options
		final Utils.Arguments params = Utils.parseCmdLineArguments(args);

		final String indir = params.inputDirectory;
		final String outdir = params.outputDirectory;

		final File prgExecResultsFile = new File(outdir, "messages.xml");
		/* Parameters */
		Integer exitStatus = 0;

		final ProgramExecutionResult executionResult = new ProgramExecutionResult();

		// The idea of the following code wrt. errors is to collect as many errors as possible before computing
		// the weighted sum, to the user's benefit since he/she then gets all of them after a single call.

		// this object is where XMCDA items will be put into.
		XMCDA xmcda = new XMCDA();

		org.xmcda.v2.XMCDA xmcda_v2 = new org.xmcda.v2.XMCDA();

		/* Read the alternatives.xml, if any */
		final File alternativesFile = new File(indir, "alternatives.xml");
		if ( alternativesFile.exists() )
		{
			try
			{
				xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters()
				        .addAll(XMCDAParser.readXMCDA(alternativesFile).getProjectReferenceOrMethodMessagesOrMethodParameters());
			}
			catch (Throwable t)
			{
				final String msg = "Unable to read & parse the file alternatives.xml, reason: ";
				executionResult.addError(Utils.getMessage(msg, t));
			}
		}

		/* Read criteria.xml, if any */
		final File criteriaFile = new File(indir, "criteria.xml");
		if ( criteriaFile.exists() )
		{
			try
			{
				xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters().addAll(XMCDAParser.readXMCDA(criteriaFile).getProjectReferenceOrMethodMessagesOrMethodParameters());
			}
			catch (Throwable t)
			{
				final String msg = "Unable to read & parse the file criteria.xml, reason: ";
				executionResult.addError(Utils.getMessage(msg, t));
			}
		}

		/* Read the performanceTable.xml */
		final File performanceTableFile = new File(indir, "performanceTable.xml");
		if ( performanceTableFile.exists() )
		{
			try
			{
				xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters().addAll(XMCDAParser.readXMCDA(performanceTableFile).getProjectReferenceOrMethodMessagesOrMethodParameters());
			}
			catch (Throwable t)
			{
				final String msg = "Unable to read & parse the file performanceTable.xml, reason: ";
				executionResult.addError(Utils.getMessage(msg, t));
			}
		}
		else
		{
			executionResult.addError("Could not find the mandatory file performanceTable.xml");
		}

		/* Read criteriaWeights.xml, if any */
		final File criteriaWeightsFile = new File(indir, "criteriaWeights.xml");
		if ( criteriaWeightsFile.exists() )
		{
			try
			{
				xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters().addAll(XMCDAParser.readXMCDA(criteriaWeightsFile).getProjectReferenceOrMethodMessagesOrMethodParameters());
			}
			catch (Throwable t)
			{
				final String msg = "Unable to read & parse the file criteriaWeights.xml, reason: ";
				executionResult.addError(Utils.getMessage(msg, t));
			}
		}

		/* Read parameters.xml, if any */
		final File parametersFile = new File(indir, "parameters.xml");
		if ( parametersFile.exists() )
		{
			try
			{
				xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters().addAll(XMCDAParser.readXMCDA(parametersFile).getProjectReferenceOrMethodMessagesOrMethodParameters());
			}
			catch (Throwable t)
			{
				final String msg = "Unable to read & parse the file parameters.xml, reason: ";
				executionResult.addError(Utils.getMessage(msg, t));
			}
		}
		else
		{
			executionResult.addError("Could not find the mandatory file parameters.xml");
		}

		/* Convert that to XMCDA v3 */
		try
		{
			xmcda = XMCDAConverter.convertTo_v3(xmcda_v2);
		}
		catch (Throwable t)
		{
			executionResult.addError(Utils.getMessage("Could not convert inputs to XMCDA v3, reason: ", t));
		}

		if ( executionResult.size() > 0 || exitStatus != 0 )
		{
			Utils.writeProgramExecutionResults(prgExecResultsFile, executionResult, Utils.XMCDA_VERSION.v2);
			return;
		}

		/* TODO à partir de là on est un moment en copy/paste de la version v3 */
		/*  Let's check the inputs */
		WeightedSum.AggregationOperator operator = null;
		final Dictionary returnDictionary = new Hashtable();
		exitStatus = Utils.handleInputs(xmcda, executionResult, returnDictionary);
		operator = (WeightedSum.AggregationOperator) returnDictionary.get("operator");

		if ( executionResult.size() != 0 || exitStatus != 0 || operator == null )
		{
			Utils.writeProgramExecutionResults(prgExecResultsFile, executionResult, Utils.XMCDA_VERSION.v2);
			return;
		}

		// Here we know that everything was loaded as expected

		/* Now let's call the calculation method */
		XMCDA result = null;
		try
		{
			result = WeightedSum.calculateWeightedSum(xmcda, operator);
		}
		catch (Throwable t)
		{
			executionResult.addError(Utils.getMessage("The calculation could not be performed, reason: ", t));
			Utils.writeProgramExecutionResults(prgExecResultsFile, executionResult, Utils.XMCDA_VERSION.v2);
			return;
		}

		/* TODO À partir de là on retourne sur du spécifique */
		final File alternativesValuesFile = new File(outdir, "alternativesValues.xml");

		org.xmcda.v2.XMCDA results_v2 = null;
		try
		{
			results_v2 = XMCDAConverter.convertTo_v2(result);
			if ( results_v2 == null )
				throw new IllegalStateException("Conversion from v3 to v2 returned a null value");
		}
		catch (Throwable t)
		{
			executionResult.addError(Utils.getMessage("Could not convert alternativesValues into XMCDA_v2, reason: ", t));
			Utils.writeProgramExecutionResults(prgExecResultsFile, executionResult, Utils.XMCDA_VERSION.v2);
			return;
		}

		try
		{
			XMCDAParser.writeXMCDA(results_v2, alternativesValuesFile);
		}
		catch (Throwable t)
		{
			executionResult.addError(Utils.getMessage("Error while writing alternativesValues.xml, reason: ", t));
				/* Whatever the error is, clean up the file: we do not want to leave an empty or partially-written file */
			alternativesValuesFile.delete();
			Utils.writeProgramExecutionResults(prgExecResultsFile, executionResult, Utils.XMCDA_VERSION.v2);
			return;
		}

		// Let's write the file 'messages.xml' as well
		Utils.writeProgramExecutionResults(prgExecResultsFile, executionResult, Utils.XMCDA_VERSION.v2);
	}

}
