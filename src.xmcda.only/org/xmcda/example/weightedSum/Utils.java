package org.xmcda.example.weightedSum;

import org.xmcda.*;
import org.xmcda.converters.v2_v3.XMCDAConverter;
import org.xmcda.parsers.xml.xmcda_v2.XMCDAParser;
import org.xmcda.utils.ValueConverters;

import java.io.File;
import java.util.Dictionary;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class Utils
{
	public enum XMCDA_VERSION
	{
		v2, v3
	}

	/**
	 * Gathers the arguments for the command line: input and output directories.
	 * @see #parseCmdLineArguments(String[])
	 */
	public static class Arguments
	{
		public String inputDirectory;
		public String outputDirectory;
	}


	/**
	 * Raised when the command line is invalid
	 *
	 * @see #parseCmdLineArguments(String[])
	 */
	public static class InvalidCommandLineException extends Exception
	{
		private static final long serialVersionUID = -3732654459819208634L;

		public InvalidCommandLineException(String message)
		{
			super(message);
		}
	}


	/**
	 * Parses the command-line and search for the input directory (options {@code -i} or {@code --input-directory})
	 * and for the output directory (options {@code -o} or {@code --output-directory}).
	 *
	 * @param args the arguments of the command-line (its length must be equal to 4).
	 * @return an {@link Arguments argument} with non-null fields {@code inputDirectory} and {@code outputDirectory}
	 * @throws InvalidCommandLineException in one or both input/output directories are not present in the command line.
	 */
	public static Arguments parseCmdLineArguments(String[] args) throws InvalidCommandLineException
	{
		// Let's make it dead simple
		if ( args.length != 4 )
			throw new InvalidCommandLineException("Invalid number of arguments");
		Arguments arguments = new Arguments();
		for ( int index = 0; index <= 2; index += 2 )
		{
			String arg = args[index];
			if ( "-i".equals(arg) || "--input-directory".equals(arg) )
				arguments.inputDirectory = args[index + 1];
			else if ( "-o".equals(arg) || "--output-directory".equals(arg) )
				arguments.outputDirectory = args[index + 1];
		}
		if ( arguments.inputDirectory == null || arguments.outputDirectory == null )
			throw new InvalidCommandLineException("Missing parameters");
		return arguments;
	}


	/**
	 * Simply returns {@link Throwable#getMessage() throwable.getMessage()}, or {@code "unknown"} if it is @{code null}.
	 *
	 * @param throwable a non-null {@link Throwable}
	 * @return the throwable's message, or "unknown" if it is null
	 */
	static String getMessage(Throwable throwable)
	{
		if ( throwable.getMessage() != null )
			return throwable.getMessage();
		// when handling XMCDA v2 files, errors may be embedded in a JAXBException
		if ( throwable.getCause() != null && throwable.getCause().getMessage() != null )
			return throwable.getCause().getMessage();
		return "unknown";
	}

	/**
	 * Simply return the provided String with {@link #getMessage(Throwable)}.
	 *
	 * @param throwable
	 * @return the concatenated String
	 */
	static String getMessage(String message, Throwable throwable)
	{
		return message + getMessage(throwable);
	}

	/**
	 * Checks the inputs
	 *
	 * @param xmcda
	 * @param errors
	 * @param returnDictionary contains a key "operator" with the appropriate {@link org.xmcda.example.WeightedSum.AggregationOperator operator}
	 */
	public static Integer handleInputs(XMCDA xmcda, ProgramExecutionResult errors, Dictionary returnDictionary)
	{
		// Get the performanceTable object
		if ( xmcda.performanceTablesList.size() == 0 )
		{
			errors.addError("No performance table has been supplied");
		}
		else if ( xmcda.performanceTablesList.size() > 1 )
		{
			errors.addError("More than one performance table has been supplied");
		}
		else if ( xmcda.performanceTablesList.get(0).isEmpty() )
		{
			errors.addError("The performance table is empty");
		}
		else
		{
			@SuppressWarnings("rawtypes")
			PerformanceTable p = xmcda.performanceTablesList.get(0);

			// Anyhow, it must be numeric here
			if ( !p.isNumeric() )
			{
				errors.addError("The performance table must contain numeric values only");
			}
			else
			{
				// convert all values as Double
				try
				{
					@SuppressWarnings("unchecked")
					PerformanceTable<Double> perfTable = p.asDouble();
					xmcda.performanceTablesList.set(0, perfTable);
				}
				catch (ValueConverters.ConversionException e)
				{
					// Should not happen because we already checked that 'p.isNumeric()' is true
					// However, bugs are always possible... so let's not ignore the throwable
					errors.addError(getMessage("Error when converting the performance table's value to Double, reason:", e));
				}
			}
		}

		// let's check the parameters
		boolean with_weights = false;

		if ( xmcda.criteriaValuesList.size() >= 1 )
			with_weights = true;
		if ( xmcda.criteriaValuesList.size() > 1 )
		{
			errors.addError("More than one criteriaValues supplied");
		}

		WeightedSum.AggregationOperator operator = null;

		/* Weights must be numeric values */
		if ( with_weights )
		{
			final CriteriaValues weights = xmcda.criteriaValuesList.get(0);
			if ( !weights.isNumeric() )
			{
				errors.addError("The weights must be numeric values only");
			}
		}

		boolean operator_parameter_is_present = false;
		if ( xmcda.programParametersList.size() > 0 && xmcda.programParametersList.get(0).size() > 0 )
		{
			for ( ProgramParameter<?> prgParam : xmcda.programParametersList.get(0) )
			{
				if ( "operator".equals(prgParam.id()) )
				{
					operator_parameter_is_present = true;
					if ( prgParam.getValues() == null || (prgParam.getValues() != null && prgParam.getValues().size() != 1) )
					{
						errors.addError("Parameter operator must have a single (label) value only");
					}
					try
					{
						final String operatorValue = (String) prgParam.getValues().get(0).getValue();
						operator = WeightedSum.AggregationOperator.fromString((String) operatorValue);
					}
					catch (Throwable throwable)
					{
						StringBuffer valid_values = new StringBuffer();
						for ( WeightedSum.AggregationOperator op : WeightedSum.AggregationOperator.values() )
						{
							valid_values.append(op.getLabel()).append(", ");
						}
						errors.addError("Invalid value for parameter operator, it must be a label, " +
												"possible values are: " + valid_values.substring(0, valid_values.length() - 2));
					}
				}
				// else TODO warning unknown parameter supplied
			}
		}
		if ( operator == null && ! operator_parameter_is_present )
			errors.addError("Mandatory parameter 'operator' is missing");
		if ( operator != null )
			returnDictionary.put("operator", operator);

		/* weights should be provided for (normalized) weighted sum */
		if ( (operator == WeightedSum.AggregationOperator.WEIGHTED_SUM || operator == WeightedSum.AggregationOperator.NORMALIZED_WEIGHTED_SUM)
				&& !with_weights )
		{
			errors.addError("Criteria weights must be supplied when parameter operator is "+operator.toString());
		}
		return errors.getStatus().exitStatus(); // TODO du coup pourquoi ne pas renvoyer directement le dictionnaire?!!
	}

	/**
	 * Writes the XMCDA file containing the information provided to build the XMCDA tag "{@code programExecutionResult}"
	 * in XMCDA v3, or "{@code methodMessages}" in XMCDA v2.x.
	 *
	 * @param prgExecResultsFile the file to write
	 * @param errors             a {@link ProgramExecutionResult} object
	 * @param xmcdaVersion       indicates which {@link XMCDA_VERSION} to use when writing the file
	 */
	public static void writeProgramExecutionResults(File prgExecResultsFile, ProgramExecutionResult errors,
													XMCDA_VERSION xmcdaVersion)
	{
		org.xmcda.parsers.xml.xmcda_v3.XMCDAParser parser = new org.xmcda.parsers.xml.xmcda_v3.XMCDAParser();

		XMCDA prgExecResults = new XMCDA();
		prgExecResults.programExecutionResultsList.add(errors);
		try
		{
			switch (xmcdaVersion)
			{
				case v3:
					parser.writeXMCDA(prgExecResults, prgExecResultsFile);
					break;
				case v2:
					org.xmcda.v2.XMCDA xmcda_v2 = XMCDAConverter.convertTo_v2(prgExecResults);
					XMCDAParser.writeXMCDA(xmcda_v2, prgExecResultsFile);
					break;
				default:
					// just in case the enum has some more values in the future and the new caseshas not been added
					throw new IllegalArgumentException("Unhandled XMCDA version " + xmcdaVersion.toString());
			}
		}
		catch (Throwable t)
		{
			// Last resort, print something on the stderr and exit
			// We choose here not to clean up the file, in case some valuable information were successfully written
			// before a throwable is raised.
			System.err.println(getMessage("Could not write messages.xml, reason: ", t));
			System.exit(-1);
		}
		if ( errors.getStatus().exitStatus() != 0 )
			System.exit(errors.getStatus().exitStatus());
	}

}
