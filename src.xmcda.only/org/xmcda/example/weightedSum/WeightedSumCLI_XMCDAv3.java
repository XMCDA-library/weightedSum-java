package org.xmcda.example.weightedSum;

import org.xmcda.ProgramExecutionResult;
import org.xmcda.XMCDA;

import java.io.File;
import java.util.Dictionary;
import java.util.Hashtable;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class WeightedSumCLI_XMCDAv3
{
	/**
	 * @param args
	 * @throws InvalidCommandLineException 
	 */
	public static void main(String[] args) throws Utils.InvalidCommandLineException
	{
		// Parsing the options
		final Utils.Arguments params = Utils.parseCmdLineArguments(args);

		final String indir = params.inputDirectory;
		final String outdir = params.outputDirectory;

		final File prgExecResults = new File(outdir, "messages.xml");

		final ProgramExecutionResult executionResult = new ProgramExecutionResult();

		// The idea of the following code wrt. errors is to collect as many errors as possible before computing
		// the weighted sum, to the user's benefit since he/she then gets all of them after a single call.

		final org.xmcda.parsers.xml.xmcda_v3.XMCDAParser parser = new org.xmcda.parsers.xml.xmcda_v3.XMCDAParser();
		// this object is where XMCDA items will be put into.
		final XMCDA xmcda = new XMCDA();

		/* Read the alternatives.xml, if any, because if it is present, it may declare some alternatives to be inactive */
		final File alternativesFile = new File(indir, "alternatives.xml");
		if ( alternativesFile.exists() )
		{
			try
			{
				parser.readXMCDA(xmcda, alternativesFile);
			}
			catch (Throwable t)
			{
				final String msg = "Unable to read & parse the file alternatives.xml, reason: ";
				executionResult.addError(Utils.getMessage(msg, t));
			}
		}
		/* Read the criteria.xml, if any */
		final File criteriaFile = new File(indir, "criteria.xml");
		if ( criteriaFile.exists() )
		{
			try
			{
				parser.readXMCDA(xmcda, criteriaFile);
			}
			catch (Throwable t)
			{
				final String msg = "Unable to read & parse the file criteria.xml, reason: ";
				executionResult.addError(Utils.getMessage(msg, t));
			}
		}

		/* Read performanceTable.xml */
		final File performanceTableFile = new File(indir, "performanceTable.xml");
		if ( performanceTableFile.exists() )
		{
			try
			{
				parser.readXMCDA(xmcda, new File(indir, "performanceTable.xml").getAbsolutePath());
			}
			catch (Throwable t)
			{
				final String msg = "Unable to read & parse the file performanceTable.xml, reason: ";
				executionResult.addError(Utils.getMessage(msg, t));
			}
		}
		else
		{
			executionResult.addError("Could not find the mandatory file performanceTable.xml");
		}

		/* Read the criteriaWeights.xml, if any */
		final File criteriaWeights = new File(indir, "criteriaWeights.xml");
		if ( criteriaWeights.exists() )
		{
			try
			{
				parser.readXMCDA(xmcda, criteriaWeights);
			}
			catch (Throwable t)
			{
				final String msg = "Unable to read & parse the file criteriaWeights.xml, reason: ";
				executionResult.addError(Utils.getMessage(msg, t));
			}
		}

		/* Read parameters.xml */
		final File parametersFile = new File(indir, "parameters.xml");
		if ( parametersFile.exists() )
		{
			try
			{
				parser.readXMCDA(xmcda, parametersFile);
			}
			catch (Throwable t)
			{
				final String msg = "Unable to read & parse the file parameters.xml, reason: ";
				executionResult.addError(Utils.getMessage(msg, t));
			}
		}
		else
		{
			executionResult.addError("Could not find the mandatory file parameters.xml");
		}

		/* We have problem with the inputs, its time to stop */
		if ( ! (executionResult.isOk() || executionResult.isWarning() ) )
		{
			Utils.writeProgramExecutionResults(prgExecResults, executionResult, Utils.XMCDA_VERSION.v3);
			return;
		}

		/*  Let's check the inputs */
		WeightedSum.AggregationOperator operator = null;
		final Dictionary returnDictionary = new Hashtable();
		Utils.handleInputs(xmcda, executionResult, returnDictionary);
		operator = (WeightedSum.AggregationOperator) returnDictionary.get("operator");

		if ( ! ( executionResult.isOk() || executionResult.isWarning() ) || operator == null )
		{
			Utils.writeProgramExecutionResults(prgExecResults, executionResult, Utils.XMCDA_VERSION.v3);
			return;
		}

		// Here we know that everything was loaded as expected

		/* Now let's call the calculation method */
		XMCDA result = null;
		try
		{
			result = WeightedSum.calculateWeightedSum(xmcda, operator);
		}
		catch (Throwable t)
		{
			executionResult.addError(Utils.getMessage("The calculation could not be performed, reason: ", t));
			Utils.writeProgramExecutionResults(prgExecResults, executionResult, Utils.XMCDA_VERSION.v3);
			return;
		}

		final File alternativesValuesFile = new File(outdir, "alternativesValues.xml");
		try
		{
			parser.writeXMCDA(result, alternativesValuesFile.getAbsolutePath());
		}
		catch (Throwable t)
		{
			executionResult.addError(Utils.getMessage("Error while writing alternativesValues.xml, reason: ", t));
			/* Whatever the error is, clean up the file: we do not want to leave an empty or partially-written file */
			alternativesValuesFile.delete();
			Utils.writeProgramExecutionResults(prgExecResults, executionResult, Utils.XMCDA_VERSION.v3);
			return;
		}

		// Let's write the file 'messages.xml' as well
		Utils.writeProgramExecutionResults(prgExecResults, executionResult, Utils.XMCDA_VERSION.v3);
	}

}
