package org.xmcda.example.weightedSum;

import java.util.ArrayList;
import java.util.Arrays;

import org.xmcda.example.weightedSum.Utils.InvalidCommandLineException;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class WeightedSumCLI
{
	public static void main(String[] args) throws InvalidCommandLineException
	{
		final ArrayList<String> argsList = new ArrayList<String>(Arrays.asList(args));
		if ( argsList.remove("--v2") )
		{
			WeightedSumCLI_XMCDAv2.main((String[]) argsList.toArray(new String[]{}));
		}
		else if ( argsList.remove("--v3") )
		{
			WeightedSumCLI_XMCDAv3.main((String[]) argsList.toArray(new String[]{}));
		}
		else
		{
			System.err.println("missing mandatory option --v2 or --v3");
			System.exit(-1);
		}
	}
}
