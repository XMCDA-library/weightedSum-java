/**
 *
 */
package org.xmcda.example.weightedSum;

import org.xmcda.*;
import org.xmcda.utils.ValueConverters.ConversionException;

import java.util.HashMap;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class WeightedSum
{
	public enum AggregationOperator
	{
		WEIGHTED_SUM("weightedSum"),
		NORMALIZED_WEIGHTED_SUM("normalizedWeightedSum"),
		SUM("sum"),
		AVERAGE("average");

		private String label;

		private AggregationOperator(String operatorLabel)
		{
			label = operatorLabel;
		}

		/**
		 * Return the label for this aggregation operator
		 *
		 * @return the operator's label
		 */
		public final String getLabel()
		{
			return label;
		}

		/**
		 * Returns the operator's label
		 *
		 * @return the operator's label
		 */
		@Override
		public String toString()
		{
			return label;
		}

		/**
		 * Returns the {@link AggregationOperator} with the specified label. It behaves like {@link #valueOf(String)}
		 * with the exception
		 *
		 * @param operatorLabel the label of the constant to return
		 * @return the enum constant with the specified label
		 * @throws IllegalArgumentException if there is no aggregation operator with this label
		 * @throws NullPointerException     if operatorLabel is null
		 */
		public static AggregationOperator fromString(String operatorLabel)
		{
			if ( operatorLabel == null )
				throw new NullPointerException("operatorLabel is null");
			for ( AggregationOperator op : AggregationOperator.values() )
			{
				if ( op.toString().equals(operatorLabel) )
					return op;
			}
			throw new IllegalArgumentException("No enum AggregationOperator with label " + operatorLabel);
		}
	}

	/**
	 * Calculates the weighted sum.
	 *
	 * @param xmcda               the XMCDA object holding the performance table and the criteria weights, if applicable.
	 *                            The latter is mandatory if the parameter {@code aggregationOperator} is either
	 *                            {@link AggregationOperator#WEIGHTED_SUM} or {@link AggregationOperator#NORMALIZED_WEIGHTED_SUM}
	 * @param aggregationOperator one of the {@link AggregationOperator}'s values.
	 * @return
	 */
	public static XMCDA calculateWeightedSum(XMCDA xmcda, AggregationOperator aggregationOperator)
	{
		HashMap<Criterion, Double> weights = new HashMap<Criterion, Double>();
		// convert all values as Double
		PerformanceTable<Double> perfTable = null;
		try
		{
			perfTable = xmcda.performanceTablesList.get(0).asDouble();
		}
		catch (ConversionException ce)
		{
			/* ... even if this should not happen since this has been verified in main() / Utils.handleInputs() */
			throw new IllegalArgumentException("One or more performance table' values are not numerical");
		}

		if ( aggregationOperator == AggregationOperator.WEIGHTED_SUM
				|| aggregationOperator == AggregationOperator.NORMALIZED_WEIGHTED_SUM )
		{
			for ( Criterion criterion : perfTable.getCriteria() )
			{
				try
				{
					weights.put(criterion, (Double) xmcda.criteriaValuesList.get(0).get(criterion).get(0).convertTo(Double.class).getValue());
				}
				catch (ConversionException e)
				{
					throw new IllegalArgumentException("One or more criteria' weights are not numerical");
				}
			}
			if ( aggregationOperator == AggregationOperator.NORMALIZED_WEIGHTED_SUM )
			{
				Double sum = 0.0;
				for ( Double v : weights.values() )
				{
					sum += v;
				}
				for ( Criterion c : weights.keySet() )
				{
					weights.put(c, weights.get(c) / sum);
				}
			}
		}
		else
		{
			// either 'average' or a simple sum: 
			for ( Criterion criterion : perfTable.getCriteria() )
			{
				if ( aggregationOperator == AggregationOperator.AVERAGE )
					weights.put(criterion, 1.0 / perfTable.getCriteria().size());
				else
					weights.put(criterion, 1.0);
			}
		}

		// Ok, calculate the weightedSum
		AlternativesValues<Double> alternativesValues = new AlternativesValues<Double>();

		for ( Alternative alternative : perfTable.getAlternatives() )
		{
			if ( !alternative.isActive() )
				continue;
			for ( Criterion criterion : perfTable.getCriteria() )
			{
				// Get the value attached to the alternative, create it if necessary
				LabelledQValues<Double> value_a = alternativesValues.setDefault(alternative, 0.0);

				// calculation
				Double value = value_a.get(0).getValue();
				//System.out.println("criterion "+criterion.id()+" weights:"+weights.get(criterion));
				value = value + (weights.get(criterion) * perfTable.getValue(alternative, criterion));
				value_a.get(0).setValue(value);
			}
		}
		// We create an other XMCDA object to store the result and write the file
		XMCDA result = new XMCDA();
		result.alternativesValuesList.add(alternativesValues);
		return result;
	}

}
